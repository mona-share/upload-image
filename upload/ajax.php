<?php 
    function upload_post_img() {
        $f =  $_POST['data']; 
        
        $data = getimagesize($f);
    
        $file = array(
            'name' => 'user-upload-'.rand().'.'.explode('/',$data['mime'])[1],
            'base' => $f,
            'type' =>  $data['mime'],
            'size' => $data['bits'],
        );   
        $up = (new Mona_upload( ))->mona_upload_image_base64( $file );
        
        $output = array('file_id' => $up, 'url' => wp_get_attachment_image_url($up, 'full') , 'status' => 'success', 'messenger' => __('Thay đổi thành công' , 'monamedia') )   ;
        if( $up != '') { 
            update_user_meta( get_current_user_id(), '_avatar', $up );
        }
        echo json_encode($output);
        wp_die();
    } 
    add_action('wp_ajax_mona_ajax_upload_post_img', [$this ,'upload_post_img']); // co dang nhap